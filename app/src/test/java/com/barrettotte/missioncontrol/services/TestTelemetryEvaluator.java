package com.barrettotte.missioncontrol.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;
import com.barrettotte.missioncontrol.enums.TelemetrySeverityType;
import com.barrettotte.missioncontrol.exception.TelemetryBaseException;
import com.barrettotte.missioncontrol.models.SatelliteAlert;
import com.barrettotte.missioncontrol.models.SatelliteComponent;
import com.barrettotte.missioncontrol.models.TelemetryRecord;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestTelemetryEvaluator {

    private TelemetryEvaluator evaluator;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        evaluator = new TelemetryEvaluator();
        objectMapper = JsonMapper.builder().findAndAddModules().build();
    }

    private TelemetryRecord getTestTelemetryRecord(final TelemetryComponentType comp, final Float raw) {
        final TelemetryRecord record = new TelemetryRecord();
        
        record.setTimestamp(LocalDateTime.of(2018, Month.JANUARY, 1, 23, 1, 9, 521000000));
        record.setSatelliteID(1000);

        if (comp.equals(TelemetryComponentType.BATT)) {
            record.setLimits(new HashMap<>(){{
                put(TelemetrySeverityType.RED_HIGH, 17);
                put(TelemetrySeverityType.YELLOW_HIGH, 15);
                put(TelemetrySeverityType.YELLOW_LOW, 9);
                put(TelemetrySeverityType.RED_LOW, 8);
            }});
        } else {
            record.setLimits(new HashMap<>(){{
                put(TelemetrySeverityType.RED_HIGH, 101);
                put(TelemetrySeverityType.YELLOW_HIGH, 98);
                put(TelemetrySeverityType.YELLOW_LOW, 25);
                put(TelemetrySeverityType.RED_LOW, 20);
            }});
        }
        record.setRawValue(raw);
        record.setComponentType(comp);
        return record;
    }

    @Test
    public void testEvalBatteryWarnings_Alert() {
        final SatelliteComponent battery = new SatelliteComponent(TelemetryComponentType.BATT);
        final TelemetryRecord record = getTestTelemetryRecord(TelemetryComponentType.BATT, 7.5f);

        battery.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.BATT, 7.5f));
        battery.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.BATT, 7.5f));

        final SatelliteAlert alert = evaluator.evalBatteryWarnings(record, battery);
        assertNotNull(alert);
        assertEquals(1000, alert.getSatelliteID());
        assertEquals(TelemetrySeverityType.RED_LOW, alert.getSeverity());
        assertEquals(TelemetryComponentType.BATT, alert.getComponent());
        assertEquals(record.getTimestamp(), alert.getTimestamp());
    }

    @Test
    public void testEvalBatteryWarnings_None() {
        final SatelliteComponent battery = new SatelliteComponent(TelemetryComponentType.BATT);
        final TelemetryRecord record = getTestTelemetryRecord(TelemetryComponentType.BATT, 7.5f);

        battery.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.BATT, 7.5f));
        battery.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.BATT, 7.5f));
        record.setTimestamp(record.getTimestamp().plusHours(1));

        final SatelliteAlert alert = evaluator.evalBatteryWarnings(record, battery);
        assertNull(alert);
    }

    @Test
    public void testEvalThermostatWarnings_Alert() {
        final SatelliteComponent thermo = new SatelliteComponent(TelemetryComponentType.TSTAT);
        final TelemetryRecord record = getTestTelemetryRecord(TelemetryComponentType.TSTAT, 128.5f);

        thermo.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.TSTAT, 128.5f));
        thermo.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.TSTAT, 128.5f));

        final SatelliteAlert alert = evaluator.evalThermostatWarnings(record, thermo);
        assertNotNull(alert);
        assertEquals(1000, alert.getSatelliteID());
        assertEquals(TelemetrySeverityType.RED_HIGH, alert.getSeverity());
        assertEquals(TelemetryComponentType.TSTAT, alert.getComponent());
        assertEquals(record.getTimestamp(), alert.getTimestamp());
    }

    @Test
    public void testEvalThermostatWarnings_None() {
        final SatelliteComponent thermo = new SatelliteComponent(TelemetryComponentType.TSTAT);
        final TelemetryRecord record = getTestTelemetryRecord(TelemetryComponentType.TSTAT, 128.5f);

        thermo.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.TSTAT, 128.5f));
        thermo.getWarnings().add(getTestTelemetryRecord(TelemetryComponentType.TSTAT, 128.5f));
        record.setTimestamp(record.getTimestamp().plusHours(1));

        final SatelliteAlert alert = evaluator.evalBatteryWarnings(record, thermo);
        assertNull(alert);
    }

    @Test
    public void testEvaluateFromFile() {
        final InputStream telemetryStream = getClass().getClassLoader().getResourceAsStream("test.txt");
        assertNotNull(telemetryStream);

        try (final ByteArrayOutputStream alertStream = new ByteArrayOutputStream()) {
            evaluator.evaluateStream(telemetryStream, alertStream);
            byte[] alertBytes = alertStream.toByteArray();

            final SatelliteAlert[] alerts = objectMapper.readValue(alertBytes, SatelliteAlert[].class);
            assertEquals(2, alerts.length);

            assertEquals(1000, alerts[0].getSatelliteID());
            assertEquals(TelemetrySeverityType.RED_HIGH, alerts[0].getSeverity());
            assertEquals(TelemetryComponentType.TSTAT, alerts[0].getComponent());
            assertEquals(LocalDateTime.of(2018, Month.JANUARY, 1, 23, 1, 38, 1000000), alerts[0].getTimestamp());

            assertEquals(1000, alerts[1].getSatelliteID());
            assertEquals(TelemetrySeverityType.RED_LOW, alerts[1].getSeverity());
            assertEquals(TelemetryComponentType.BATT, alerts[1].getComponent());
            assertEquals(LocalDateTime.of(2018, Month.JANUARY, 1, 23, 1, 9, 521000000), alerts[1].getTimestamp());

        } catch (final IOException e) {
            e.printStackTrace();
            fail(String.format("Unexpected IOException\n\t%s\n", e.getMessage()));
        } catch (final TelemetryBaseException e) {
            e.printStackTrace();
            fail(String.format("Unexpected Telemetry Exception", e.getMessage()));
        }
    }
}
