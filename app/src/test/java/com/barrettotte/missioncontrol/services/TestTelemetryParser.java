package com.barrettotte.missioncontrol.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDateTime;
import java.time.Month;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;
import com.barrettotte.missioncontrol.enums.TelemetrySeverityType;
import com.barrettotte.missioncontrol.exception.TelemetryParseException;
import com.barrettotte.missioncontrol.models.TelemetryRecord;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestTelemetryParser {
    
    private TelemetryParser parser;

    @BeforeEach
    public void setup() {
        parser = new TelemetryParser();
    }

    @Test
    public void testParseBattery() {
        final String row = "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT";
        final LocalDateTime ts = LocalDateTime.of(2018, Month.JANUARY, 1, 23, 1, 9, 521000000);

        try {
            final TelemetryRecord record = parser.parseTelemetryRecord(row);

            assertEquals(ts, record.getTimestamp());
            assertEquals(1000, record.getSatelliteID());
            
            assertEquals(4, record.getLimits().size());
            assertEquals(17, record.getLimits().get(TelemetrySeverityType.RED_HIGH));
            assertEquals(15, record.getLimits().get(TelemetrySeverityType.YELLOW_LOW));
            assertEquals(9, record.getLimits().get(TelemetrySeverityType.YELLOW_HIGH));
            assertEquals(8, record.getLimits().get(TelemetrySeverityType.RED_LOW));

            assertEquals(7.8f, record.getRawValue());
            assertEquals(TelemetryComponentType.BATT, record.getComponentType());

        } catch (final TelemetryParseException e) {
            e.printStackTrace();
            fail(String.format("Unexpected Telemetry parse exception", e.getMessage()));
        }
    }

    @Test
    public void testParseThermostat() {
        final String row = "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT";
        final LocalDateTime ts = LocalDateTime.of(2018, Month.JANUARY, 1, 23, 1, 5, 1000000);
        
        try {
            final TelemetryRecord record = parser.parseTelemetryRecord(row);

            assertEquals(ts, record.getTimestamp());
            assertEquals(1001, record.getSatelliteID());
            
            assertEquals(4, record.getLimits().size());
            assertEquals(101, record.getLimits().get(TelemetrySeverityType.RED_HIGH));
            assertEquals(98, record.getLimits().get(TelemetrySeverityType.YELLOW_LOW));
            assertEquals(25, record.getLimits().get(TelemetrySeverityType.YELLOW_HIGH));
            assertEquals(20, record.getLimits().get(TelemetrySeverityType.RED_LOW));

            assertEquals(99.9f, record.getRawValue());
            assertEquals(TelemetryComponentType.TSTAT, record.getComponentType());

        } catch (final TelemetryParseException e) {
            e.printStackTrace();
            fail(String.format("Unexpected Telemetry parse exception", e.getMessage()));
        }
    }

    @Test
    public void testParseBadData() {
        final String row = "20180101 23:01:05.001|1001|101|98|25|20|99.9|????";
        final LocalDateTime ts = LocalDateTime.of(2018, Month.JANUARY, 1, 23, 1, 5, 1000000);
        Exception expected = null;

        try {
            final TelemetryRecord record = parser.parseTelemetryRecord(row);

            assertEquals(ts, record.getTimestamp());
            assertEquals(1001, record.getSatelliteID());
            
            assertEquals(4, record.getLimits().size());
            assertEquals(101, record.getLimits().get(TelemetrySeverityType.RED_HIGH));
            assertEquals(98, record.getLimits().get(TelemetrySeverityType.YELLOW_LOW));
            assertEquals(25, record.getLimits().get(TelemetrySeverityType.YELLOW_HIGH));
            assertEquals(20, record.getLimits().get(TelemetrySeverityType.RED_LOW));

            assertEquals(99.9f, record.getRawValue());
            assertEquals(TelemetryComponentType.TSTAT, record.getComponentType());

        } catch (final TelemetryParseException e) {
            expected = e;
        }
        assertNotNull(expected);
    }
}
