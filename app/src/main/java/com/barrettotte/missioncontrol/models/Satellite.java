package com.barrettotte.missioncontrol.models;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;

public class Satellite {
    
    private Integer id;
    private SatelliteComponent battery;
    private SatelliteComponent thermostat;

    public Satellite(final Integer id) {
        this.id = id;
        this.battery = new SatelliteComponent(TelemetryComponentType.BATT);
        this.thermostat = new SatelliteComponent(TelemetryComponentType.TSTAT);
    }

    public Satellite(final Integer id, final SatelliteComponent battery, final SatelliteComponent thermostat) {
        this.id = id;
        this.battery = battery;
        this.thermostat = thermostat;
    }

    public Integer getID() {
        return this.id;
    }

    public void setID(final Integer id) {
        this.id = id;
    }

    public SatelliteComponent getBattery() {
        return this.battery;
    }

    public void setBattery (final SatelliteComponent battery) {
        this.battery = battery;
    }

    public SatelliteComponent getThermostat() {
        return this.thermostat;
    }

    public void setThermostat(final SatelliteComponent thermostat) {
        this.thermostat = thermostat;
    }
}
