package com.barrettotte.missioncontrol.models;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;
import com.barrettotte.missioncontrol.enums.TelemetrySeverityType;

public class TelemetryRecord {
    
    private LocalDateTime timestamp;
    private Integer satelliteID;
    private Map<TelemetrySeverityType, Integer> limits;
    private Float rawValue;
    private TelemetryComponentType componentType;

    public TelemetryRecord() {
        this.limits = new HashMap<>();
    }

    public TelemetryRecord(
        final LocalDateTime timestamp, 
        final Integer satelliteID, 
        final Map<TelemetrySeverityType, Integer> limits, 
        final Float rawValue,
        final TelemetryComponentType componentType
    ) {
        this.timestamp = timestamp;
        this.satelliteID = satelliteID;
        this.limits = limits;
        this.componentType = componentType;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getSatelliteID() {
        return this.satelliteID;
    }

    public void setSatelliteID(final Integer satelliteID) {
        this.satelliteID = satelliteID;
    }

    public Map<TelemetrySeverityType, Integer> getLimits() {
        return this.limits;
    }

    public void setLimits(final Map<TelemetrySeverityType, Integer> limits) {
        this.limits = limits;
    }

    public Float getRawValue() {
        return this.rawValue;
    }

    public void setRawValue(final Float rawValue) {
        this.rawValue = rawValue;
    }

    public TelemetryComponentType getComponentType() {
        return this.componentType;
    }

    public void setComponentType(final TelemetryComponentType componentType) {
        this.componentType = componentType;
    }
}
