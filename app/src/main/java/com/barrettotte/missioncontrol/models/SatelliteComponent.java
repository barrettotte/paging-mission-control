package com.barrettotte.missioncontrol.models;

import java.util.LinkedList;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;

public class SatelliteComponent {

    private TelemetryComponentType type;
    private LinkedList<TelemetryRecord> warnings;

    public SatelliteComponent(final TelemetryComponentType type) {
        this.type = type;
        this.warnings = new LinkedList<>();
    }

    public TelemetryComponentType getType() {
        return this.type;
    }

    public void setType(final TelemetryComponentType type) {
        this.type = type;
    }

    public LinkedList<TelemetryRecord> getWarnings() {
        return this.warnings;
    }

    public void setWarnings(final LinkedList<TelemetryRecord> warnings) {
        this.warnings = warnings;
    }
}
