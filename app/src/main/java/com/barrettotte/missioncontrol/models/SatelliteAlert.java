package com.barrettotte.missioncontrol.models;

import java.time.LocalDateTime;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;
import com.barrettotte.missioncontrol.enums.TelemetrySeverityType;
import com.fasterxml.jackson.annotation.JsonFormat;

public class SatelliteAlert implements Comparable<SatelliteAlert> {
    
    private Integer satelliteID;
    
    private TelemetrySeverityType severity;
    private TelemetryComponentType component;

    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="UTC")
    private LocalDateTime timestamp;

    public SatelliteAlert() { }

    public SatelliteAlert(
        final Integer satelliteID, 
        final TelemetrySeverityType severity,
        final TelemetryComponentType component,
        final LocalDateTime timestamp
    ) {
        this.satelliteID = satelliteID;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public Integer getSatelliteID() {
        return this.satelliteID;
    }

    public void setSatelliteID(final Integer satelliteID) {
        this.satelliteID = satelliteID;
    }

    public TelemetrySeverityType getSeverity() {
        return this.severity;
    }

    public void setSeverityType(final TelemetrySeverityType severity) {
        this.severity = severity;
    }

    public TelemetryComponentType getComponent() {
        return this.component;
    }

    public void setComponentType(final TelemetryComponentType component) {
        this.component = component;
    }

    public LocalDateTime getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(final SatelliteAlert other) {
        return this.getTimestamp().compareTo(other.getTimestamp());
    }

}
