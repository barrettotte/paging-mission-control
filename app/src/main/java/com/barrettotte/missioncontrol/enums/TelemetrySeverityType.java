package com.barrettotte.missioncontrol.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TelemetrySeverityType {
    
    YELLOW_LOW(),
    YELLOW_HIGH(),
    RED_LOW(),
    RED_HIGH();

    @JsonValue
    public final String displayText;

    private TelemetrySeverityType() {
        this.displayText = this.name().replace("_", " ");
    }

    private TelemetrySeverityType(final String displayText) {
        this.displayText = displayText;
    }

    public String getDisplayText() {
        return this.displayText;
    }
}
