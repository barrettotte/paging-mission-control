package com.barrettotte.missioncontrol.enums;

public enum TelemetryComponentType {
    BATT,
    TSTAT
}
