package com.barrettotte.missioncontrol.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;
import com.barrettotte.missioncontrol.enums.TelemetrySeverityType;
import com.barrettotte.missioncontrol.exception.TelemetryParseException;
import com.barrettotte.missioncontrol.models.TelemetryRecord;

public class TelemetryParser {

    private List<TelemetrySeverityType> severityLimitsOrder;
    private DateTimeFormatter timestampFormatter;

    public TelemetryParser() {
        this.severityLimitsOrder = Arrays.asList(
            TelemetrySeverityType.RED_HIGH,
            TelemetrySeverityType.YELLOW_LOW,
            TelemetrySeverityType.YELLOW_HIGH,
            TelemetrySeverityType.RED_LOW
        );
        this.timestampFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
    }

    public TelemetryParser(final List<TelemetrySeverityType> severityLimitsOrder, 
        final DateTimeFormatter timestampFormatter
    ) {
        this.severityLimitsOrder = severityLimitsOrder;
        this.timestampFormatter = timestampFormatter;
    }

    /** 
     * parse a line of telemetry data of format:
     *     <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>
     *         |<red-low-limit>|<raw-value>|<component>
     *
     * into a telemetry record.
     * 
     * @param telemetryRow
     * @return TelemetryRecord
     * @throws TelemetryParseException
    */
    public TelemetryRecord parseTelemetryRecord(final String telemetryRow) throws TelemetryParseException {
        
        final List<String> fields = Arrays.asList(telemetryRow.split("\\|"));
        final TelemetryRecord record = new TelemetryRecord();
        int fieldIdx = 0;

        try{
            record.setTimestamp(LocalDateTime.parse(fields.get(fieldIdx++), timestampFormatter));
            record.setSatelliteID(Integer.parseInt(fields.get(fieldIdx++)));
            
            for (final TelemetrySeverityType sev : this.severityLimitsOrder) {
                record.getLimits().put(sev, Integer.parseInt(fields.get(fieldIdx++)));
            }

            record.setRawValue(Float.parseFloat(fields.get(fieldIdx++)));
            record.setComponentType(TelemetryComponentType.valueOf(fields.get(fieldIdx++)));
            
        } catch (final NumberFormatException e) {
            throw new TelemetryParseException("Unable to parse number in telemetry data", e);
        } catch (final Exception e) {
            throw new TelemetryParseException("Unhandled exception encountered.", e);
        }
        return record;
    }
}
