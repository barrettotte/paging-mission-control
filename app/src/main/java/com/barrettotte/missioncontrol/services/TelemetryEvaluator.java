package com.barrettotte.missioncontrol.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.barrettotte.missioncontrol.enums.TelemetryComponentType;
import com.barrettotte.missioncontrol.enums.TelemetrySeverityType;
import com.barrettotte.missioncontrol.exception.TelemetryEvalException;
import com.barrettotte.missioncontrol.exception.TelemetryParseException;
import com.barrettotte.missioncontrol.models.Satellite;
import com.barrettotte.missioncontrol.models.SatelliteAlert;
import com.barrettotte.missioncontrol.models.SatelliteComponent;
import com.barrettotte.missioncontrol.models.TelemetryRecord;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

public class TelemetryEvaluator {

    private ObjectMapper objectMapper;
    private TelemetryParser parser;

    public TelemetryEvaluator() {
        this.parser = new TelemetryParser();
        this.objectMapper = initObjectMapper();
    }

    public TelemetryEvaluator(final ObjectMapper objectMapper) {
        this.parser = new TelemetryParser();
        this.objectMapper = objectMapper;
    }

    /** 
     * evaluate telemetry input stream and write to output stream
     * 
     * @param telemetryStream
     * @param resultStream
     * @throws IOException
     * @throws TelemetryParseException
     * @throws TelemetryEvalException
    */
    public void evaluateStream(final InputStream telemetryStream, final OutputStream alertStream) 
        throws IOException, TelemetryParseException, TelemetryEvalException 
    {
        try (
            final Reader telemetryReader = new InputStreamReader(telemetryStream, StandardCharsets.US_ASCII);
            final BufferedReader telemetryBuffer = new BufferedReader(telemetryReader);
            final Writer alertWriter = new OutputStreamWriter(alertStream);
            final BufferedWriter alertBuffer = new BufferedWriter(alertWriter)
        ) {
            final Map<Integer, Satellite> satellites = new HashMap<>();  // ID,Satellite
            final List<SatelliteAlert> alerts = new ArrayList<>();
            String line;
            
            while ((line = telemetryBuffer.readLine()) != null) {
                final TelemetryRecord telRecord = this.parser.parseTelemetryRecord(line);
                final Integer satelliteID = telRecord.getSatelliteID();

                // add new satellite if not previously found
                if (!satellites.containsKey(satelliteID)) {
                    satellites.put(satelliteID, new Satellite(satelliteID));
                }
                final Satellite satellite = satellites.get(satelliteID);

                // evaluate business logic per component
                final TelemetryComponentType compType = telRecord.getComponentType();
                SatelliteAlert alert = null;
                
                switch(compType) {
                    case BATT:
                        alert = evalBatteryWarnings(telRecord, satellite.getBattery());
                        break;
                    case TSTAT:
                        alert = evalThermostatWarnings(telRecord, satellite.getThermostat());
                        break;
                    default:
                        throw new UnsupportedOperationException(String.format("Unimplemented component type %s", compType.name()));
                }

                // record alert
                if (alert != null) {
                    alerts.add(alert);
                }
            }

            // write alerts as json into output stream
            final String alertsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(alerts);
            alertBuffer.write(alertsJson);

        } catch (final UnsupportedOperationException e) {
            throw new TelemetryEvalException(
                String.format("Unsupported functionality was encountered.\n\t%s", e.getMessage()), e);
        } catch (final Exception e) {
            throw new TelemetryEvalException("Unhandled exception encountered.", e);
        }
    }
    
    /**
     * evaluate business logic on battery component of a satellite
     * 
     * @param record
     * @param battery
     * @return SatelliteAlert
    */
    public SatelliteAlert evalBatteryWarnings(final TelemetryRecord record, final SatelliteComponent battery) {
        
        if (record.getRawValue() < record.getLimits().get(TelemetrySeverityType.RED_LOW)) {
            battery.getWarnings().add(record);
        }
        return checkForAlert(battery, TelemetrySeverityType.RED_LOW, 3, 5);
    }

    /** 
     * evaluate business logic on thermostat component of a satellite
     * 
     * @param record
     * @param thermostat
     * @return SatelliteAlert
    */
    public SatelliteAlert evalThermostatWarnings(final TelemetryRecord record, final SatelliteComponent thermostat) {

        if (record.getRawValue() > record.getLimits().get(TelemetrySeverityType.RED_HIGH)) {
            thermostat.getWarnings().add(record);
        }
        return checkForAlert(thermostat, TelemetrySeverityType.RED_HIGH, 3, 5);
    }

    /** 
     * initialize and configure a new object mapper
     * 
     * @return ObjectMapper
    */
    private ObjectMapper initObjectMapper() {
        final ObjectMapper objectMapper = JsonMapper.builder().findAndAddModules().build();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.enable(SerializationFeature.FLUSH_AFTER_WRITE_VALUE);

        final DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
        printer.indentObjectsWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
        printer.indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
        objectMapper.setDefaultPrettyPrinter(printer);

        return objectMapper;
    }

    /** 
     * check list of warnings and alert if past given thresholds
     * 
     * @param component
     * @param severity
     * @param amountThreshold
     * @param periodMins
     * @return SatelliteAlert
    */
    private SatelliteAlert checkForAlert(final SatelliteComponent component, final TelemetrySeverityType severity,
        final Integer amountThreshold, final Integer periodMins
    ) {
        SatelliteAlert alert = null;

        // check if threshold reached
        if (component.getWarnings().size() >= amountThreshold) {
            final LocalDateTime frameEnd = component.getWarnings()
                .getFirst().getTimestamp().plusMinutes(periodMins);

            // gather warnings within timeframe
            final int warningCount = component.getWarnings()
                .stream()
                .filter(w -> w.getTimestamp().isBefore(frameEnd))
                .collect(Collectors.toList()).size();
            
            if (warningCount >= amountThreshold) {
                final TelemetryRecord record = component.getWarnings().removeFirst();
                
                // map record to alert
                alert = new SatelliteAlert(record.getSatelliteID(), severity, 
                    record.getComponentType(), record.getTimestamp());
                component.getWarnings().subList(0, warningCount-1).clear();  // clear out warnings already used
            }
        }
        return alert;
    }
}
