package com.barrettotte.missioncontrol;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileSystems;

import com.barrettotte.missioncontrol.exception.TelemetryEvalException;
import com.barrettotte.missioncontrol.exception.TelemetryParseException;
import com.barrettotte.missioncontrol.services.TelemetryEvaluator;

public class App {

    public static void main(final String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java mission-control.jar <absolute-path>");
            System.exit(1);
        }

        // get absolute path from user input
        final String filePath = args[0].replaceFirst("^~", System.getProperty("user.home"));
        final File telemetryFile = FileSystems.getDefault().getPath(filePath).toFile();
        
        // attempt to evaluate telemetry
        try(
            final FileInputStream inStream = new FileInputStream(telemetryFile);
            final BufferedInputStream inBuffer = new BufferedInputStream(inStream);
            final PrintStream outStream = new PrintStream(System.out);
            final BufferedOutputStream outBuffer = new BufferedOutputStream(outStream)
        ){
            final TelemetryEvaluator telemetryEvaluator = new TelemetryEvaluator();
            telemetryEvaluator.evaluateStream(inBuffer, outBuffer);

        } catch (final IOException e) {
            System.out.println("Failed to evaluate telemetry, I/O exception occurred.");
            System.exit(2);
        } catch (final TelemetryParseException|TelemetryEvalException e) {
            System.out.printf("Failed to evaluate telemetry file.\n\t%s\n", e.getMessage());
            System.exit(3);
        }
    }
}
