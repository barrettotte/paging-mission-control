package com.barrettotte.missioncontrol.exception;

public class TelemetryParseException extends TelemetryBaseException {
    
    public TelemetryParseException(final String msg) {
        super(msg);
    }

    public TelemetryParseException(final String msg, final Exception rootException) {
        super(msg, rootException);
    }
}
