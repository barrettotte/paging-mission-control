package com.barrettotte.missioncontrol.exception;

public class TelemetryBaseException extends Exception {

    private Exception rootException;

    public TelemetryBaseException(final String msg) {
        super(msg);
    }

    public TelemetryBaseException(final String msg, final Exception rootException) {
        super(msg);
        this.rootException = rootException;
    }

    public Exception getRootException() {
        return this.rootException;
    }
}
