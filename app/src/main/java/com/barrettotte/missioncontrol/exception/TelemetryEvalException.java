package com.barrettotte.missioncontrol.exception;

public class TelemetryEvalException extends TelemetryBaseException {
    
    public TelemetryEvalException(final String msg) {
        super(msg);
    }

    public TelemetryEvalException(final String msg, final Exception rootException) {
        super(msg, rootException);
    }
}
